import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split

from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import plot_tree  # to visualize tree
from sklearn.ensemble import RandomForestClassifier
import keras
from keras.layers import Dense
from sklearn.metrics import accuracy_score

# importing data from repository
df = pd.read_csv('./data/smoke.csv', header=0, sep=';')

# checking if dataset has labels, shape, column type, data summary, unique, nan
print(f'\n------------> DATASET HEAD <------------\n{df.head()}')
print(f'\n------------> DATASET INFO <------------')
print(df.info())
print(f'\n------------> DATASET DESCRIPTION <------------\n{df.describe().round(2)}')
print(f'\n------------> UNIQUE VALUES <------------\n{df.nunique().sort_values()}')
print(f'\n------------> DUPLICATED VALUES <------------\n{df.duplicated().sum()}, ({np.round(100*df.duplicated().sum()/len(df),2)}%)')
print(f'\n------------> NAN VALUES <------------\n{df.isnull().sum()}')
dfa = df.dropna()
print(f'\n------------> NAN VALUES AFTER DROP <------------\n{dfa.isnull().sum()}')
print(f'SHAPE: {dfa.shape}')

# dataset visualizations
smokers = df.smoking.sum()
nonsmokers = df.shape[0] - smokers
plt.pie([smokers, nonsmokers], labels=['smokers', 'non-smokers'], autopct='%1.0f%%')
plt.show()

male = df.gender.value_counts()['M']
female = df.gender.value_counts()['F']
plt.pie([male, female], labels=['Male', 'Female'], autopct='%1.0f%%')
plt.show()

f, (ax_box, ax_hist) = plt.subplots(2, sharex=True, gridspec_kw={"height_ratios": (.15, .85)})
sns.boxplot(df["age"], ax=ax_box)
sns.histplot(data=df["age"], bins=14, ax=ax_hist)
ax_box.set(xlabel='')
plt.show()

f, (ax_box, ax_hist) = plt.subplots(2, sharex=True, gridspec_kw={"height_ratios": (.15, .85)})
sns.boxplot(df["height(cm)"], ax=ax_box)
sns.histplot(data=df["height(cm)"], bins=12, ax=ax_hist)
ax_box.set(xlabel='')
plt.show()

f, (ax_box, ax_hist) = plt.subplots(2, sharex=True, gridspec_kw={"height_ratios": (.15, .85)})
sns.boxplot(df["weight(kg)"], ax=ax_box)
sns.histplot(data=df["weight(kg)"], bins=20, ax=ax_hist)
ax_box.set(xlabel='')
plt.show()

# scaling and enconding
print(dfa.columns)
# dropping column ID
dfa = dfa.drop(labels='ID', axis=1)

# encoding column gender, tartar
dfa['gender'] = dfa['gender'].replace('F', 0)
dfa['gender'] = dfa['gender'].replace('M', 1)
dfa['tartar'] = dfa['tartar'].replace('Y', 1)
dfa['tartar'] = dfa['tartar'].replace('N', 0)

# min-max scaling columns
data = dfa.to_numpy(dtype='float64')
scaler = MinMaxScaler()
data = scaler.fit_transform(data)

# train test split
x = data[:, :-1]
y = data[:, -1]

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=1997)

SVC_lin = SVC(kernel='linear')
SVC_lin.fit(x_train, y_train)
svm_pred = SVC_lin.predict(x_test)
accuracy = accuracy_score(y_test, svm_pred)
print(f'Linear SVM Accuracy: {100 * accuracy: .2f}%')

SVC_rbf = SVC(kernel='rbf')
SVC_rbf.fit(x_train, y_train)
svc_pred = SVC_rbf.predict(x_test)
svc_accuracy = accuracy_score(y_test, svc_pred)
print(f'RBF SVC Accuracy DEFAULT: {100 * svc_accuracy: .2f}%')

knn = KNN()
knn.fit(x_train, y_train)
knn_pred = knn.predict(x_test)
knn_accuracy = accuracy_score(y_test, knn_pred)
print(f'KNN Accuracy Default: {100 * knn_accuracy: .2f}%')

gini_model = DecisionTreeClassifier()
gini_model.fit(x_train, y_train)
y_gini_model = gini_model.predict(x_test)
gini_acc = accuracy_score(y_test, y_gini_model)
print(f'GINI accuracy ID3: {gini_acc * 100:.2f}%')
plt.figure(figsize=(12, 12))
plot_tree(gini_model, filled=True, fontsize=6, feature_names=dfa.columns)
plt.show()

ent_model = DecisionTreeClassifier(criterion='entropy')
ent_model.fit(x_train, y_train)
y_ent_model = ent_model.predict(x_test)
ent_acc = accuracy_score(y_test, y_ent_model)
print(f'ENTROPY accuracy ID3: {ent_acc * 100:.2f}%')
plt.figure(figsize=(12, 12)) # width, height
plot_tree(ent_model, filled=True, fontsize=6, feature_names=dfa.columns)
plt.show()

rf_model = RandomForestClassifier(n_estimators=100)
rf_model.fit(x_train, y_train)
y_rf_model = rf_model.predict(x_test)
rf_acc = accuracy_score(y_test, y_rf_model)
print(f'RANDOM FOREST accuracy: {rf_acc * 100:.2f}%')

N, M = 100, 10
model = keras.Sequential(layers=[
    Dense(units=N, activation='relu', input_dim=20),
    Dense(units=M, activation='relu'),
    Dense(units=1, activation='relu')
])
print(model.summary())

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(x_train, y_train, batch_size=250, epochs=50)
y_pred = model.predict(x_test)
y_pred = np.argmax(y_pred, axis=1)
print(f'NN Accuracy: {np.mean(y_pred == y_test) * 100}%')
