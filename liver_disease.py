import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.tree import plot_tree  # visualizzare albero
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import LabelEncoder
from matplotlib import pyplot as plt
from sklearn.preprocessing import normalize

columns_name = ['Age', 'Gender', 'Total_Bilirubin', 'Direct_Bilirubin', 'Alkaline_Phosphotase',
                'Alamine_Aminotransferase', 'Aspartate_Aminotransferase',
                'Total_Protiens', 'Albumin', 'Albumin_and_Globulin_Ratio',
                'Patient']  # 1 = liver disease, 0 = NON liver disease

liver_data = pd.read_csv('./data/liver-disease.csv', header=None)
liver_data.columns = columns_name

label_encoder = LabelEncoder()
liver_data.Gender = label_encoder.fit_transform(liver_data['Gender'])  # 0=F 1=M

print(liver_data.isnull().sum())
liver_data=liver_data.dropna()
print('__________________________')
print(liver_data.isnull().sum())

liver_data = liver_data.to_numpy(dtype='float')

print(f'shape: {liver_data.shape}')
x = liver_data[:, :-1]
y = liver_data[:, -1]-1

#x = normalize(x, axis=0, norm='max')

SVM=[]
RBF=[]
KNNl=[]
GINI=[]
ENT=[]
RF=[]
for i in range(3):
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)

    cls = SVC(kernel='linear')
    cls.fit(x_train, y_train)
    y_pred = cls.predict(x_test)
    accuracy = accuracy_score(y_test, y_pred)
    print(f'Linear SVM Accuracy: {100 * accuracy: .2f}%')
    SVM.append(round(accuracy,4))

    clf2 = SVC()#kernel='rbf')
    clf2.fit(x_train, y_train)
    svc_pred = clf2.predict(x_test)
    svc_accuracy = accuracy_score(y_test, svc_pred)
    print(f'RBF SVC Accuracy DEFAULT: {100 * svc_accuracy: .2f}%')
    RBF.append(round(svc_accuracy,4))

    clf3 = KNN(7)
    clf3.fit(x_train, y_train)
    knn_pred = clf3.predict(x_test)
    knn_accuracy = accuracy_score(y_test, knn_pred)
    print(f'KNN: {100 * knn_accuracy: .2f}%')
    KNNl.append(round(knn_accuracy,4))

    gini_model = DecisionTreeClassifier()
    gini_model.fit(x_train, y_train)
    y_gini_model = gini_model.predict(x_test)
    gini_acc = accuracy_score(y_test, y_gini_model)
    print(f'GINI accuracy ID3: {gini_acc * 100:.2f}%')
    GINI.append(round(gini_acc,4))

    ent_model = DecisionTreeClassifier(criterion='entropy')
    ent_model.fit(x_train, y_train)
    y_ent_model = ent_model.predict(x_test)
    ent_acc = accuracy_score(y_test, y_ent_model)
    print(f'ENTROPY accuracy ID3: {ent_acc * 100:.2f}%')
    ENT.append(round(ent_acc,4))

    rf_model= RandomForestClassifier(max_depth=2, random_state=0)
    rf_model.fit(x_train,y_train)
    y_model=rf_model.predict(x_test)
    accuracy=accuracy_score(y_test, y_model)
    print(f'RANDOM FOREST accuracy: {ent_acc * 100:.2f}%')
    RF.append(round(accuracy,4))
    print('_________________________________')

print(f'SVM={SVM}'); print(f'RBF={RBF}');print(f'KNN={KNNl}');print(f'GINI={GINI}');print(f'ENT={ENT}');print(f'RF={RF}')
print(f'AVG SVM={(sum(SVM)/len(SVM))*100}')
print(f'AVG RBF={(sum(RBF)/len(RBF))*100}')
print(f'AVG KNN={(sum(KNNl)/len(KNNl))*100}')
print(f'AVG GINI={(sum(GINI)/len(GINI))*100}')
print(f'AVG ENT={(sum(ENT)/len(ENT))*100}')
print(f'AVG RF={(sum(RF)/len(RF))*100}')

# plt.figure(figsize=(12,12))
# plot_tree(ent_model, filled=True, fontsize=6, feature_names=columns_name)
# plt.show()
#
# plt.figure(figsize=(12,12))
# plot_tree(gini_model, filled=True, fontsize=6, feature_names=columns_name)
# plt.show()
