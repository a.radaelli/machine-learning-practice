import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.tree import plot_tree # visualizzare albero
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
# classificatore random forest
from sklearn.preprocessing import LabelEncoder
from matplotlib import pyplot as plt
from sklearn.preprocessing import normalize

diabetes_data = pd.read_csv('./data/diabetes.csv', header=0)

columns=[]
for elem in diabetes_data.columns:
    columns.append(elem)
#print(columns)

print(diabetes_data.isnull().sum())
diabetes_data=diabetes_data.dropna()
print('__________________________')
print(diabetes_data.isnull().sum())

diabetes_data = diabetes_data.to_numpy(dtype='float')
#print(f'shape: {diabetes_data.shape}')

x = diabetes_data[:, :-1]
y = diabetes_data[:, -1]

x = normalize(x, axis=0, norm='max')

# print(y)

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

cls = SVC(kernel='linear')
cls.fit(x_train, y_train)
y_pred = cls.predict(x_test)
accuracy = np.mean(y_test == y_pred)
print(f'Linear SVM Accuracy: {100 * accuracy: .2f}%')

clf2 = SVC(kernel='rbf')
clf2.fit(x_train, y_train)
svc_pred = clf2.predict(x_test)
svc_accuracy = np.mean(y_test == svc_pred)
print(f'RBF SVC Accuracy DEFAULT: {100 * svc_accuracy: .2f}%')

clf3 = KNN(5)
clf3.fit(x_train, y_train)
knn_pred = clf3.predict(x_test)
knn_accuracy = np.mean(y_test == knn_pred)
print(f'KNN: {100 * knn_accuracy: .2f}%')

gini_model = DecisionTreeClassifier()
gini_model.fit(x_train, y_train)
y_gini_model = gini_model.predict(x_test)
gini_acc = accuracy_score(y_test, y_gini_model)
print(f'GINI accuracy ID3: {gini_acc * 100:.2f}%')

ent_model = DecisionTreeClassifier(criterion='entropy')
ent_model.fit(x_train, y_train)
y_ent_model = ent_model.predict(x_test)
ent_acc = accuracy_score(y_test, y_ent_model)
print(f'ENTROPY accuracy ID3: {ent_acc * 100:.2f}%')

plt.figure(figsize=(50,30))
plot_tree(ent_model, filled=True, fontsize=6, feature_names=columns,rounded=True)
plt.savefig('ENT_decision_tree.png')
#plt.show()

plt.figure(figsize=(50,30))
plot_tree(gini_model, filled=True, fontsize=6, feature_names=columns, rounded=True)
plt.savefig('GINI_decision_tree.png')
#plt.show()
