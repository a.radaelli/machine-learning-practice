import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.tree import plot_tree # visualizzare albero
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
# classificatore random forest
from sklearn.preprocessing import LabelEncoder
from matplotlib import pyplot as plt
from sklearn.preprocessing import normalize

columns_name=["radius_mean","texture_mean","perimeter_mean","area_mean","smoothness_mean","compactness_mean","concavity_mean","concave points_mean","symmetry_mean","fractal_dimension_mean","radius_se","texture_se","perimeter_se","area_se","smoothness_se","compactness_se","concavity_se","concave points_se","symmetry_se","fractal_dimension_se","radius_worst","texture_worst","perimeter_worst","area_worst","smoothness_worst","compactness_worst","concavity_worst","concave points_worst","symmetry_worst","fractal_dimension_worst"]


cancer_data = pd.read_csv('./data/wisconsin-breast-cancer.csv', header=0)

labelencoder = LabelEncoder()
cancer_data.diagnosis = labelencoder.fit_transform(cancer_data['diagnosis'])
cancer_data.drop(columns=['Unnamed: 32', 'id'], inplace=True)
#print(cancer_data.fractal_dimension_se)
cancer_data = cancer_data.to_numpy(dtype='float')

#print(f'shape: {cancer_data.shape}')
x = cancer_data[:, 1:]  # tutte le colonne tranne l'ultima
x = normalize(x, axis=0, norm='max')
# print(x.shape)
y = cancer_data[:, 0]  # 1=M 0=B
# print(y)
# print(y.shape)
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)

cls = SVC(kernel='linear')
cls.fit(x_train, y_train)
y_pred = cls.predict(x_test)
accuracy = np.mean(y_test == y_pred)
print(f'Linear SVM Accuracy: {100 * accuracy: .2f}%')

clf2 = SVC(kernel='rbf')
clf2.fit(x_train, y_train)
svc_pred = clf2.predict(x_test)
svc_accuracy = np.mean(y_test == svc_pred)
print(f'RBF SVC Accuracy DEFAULT: {100 * svc_accuracy: .2f}%')

clf3 = KNN(5)
clf3.fit(x_train, y_train)
knn_pred = clf3.predict(x_test)
knn_accuracy = np.mean(y_test == knn_pred)
print(f'KNN: {100 * knn_accuracy: .2f}%')

gini_model = DecisionTreeClassifier()
gini_model.fit(x_train, y_train)
y_gini_model = gini_model.predict(x_test)
gini_acc = accuracy_score(y_test, y_gini_model)
print(f'GINI accuracy ID3: {gini_acc * 100:.2f}%')

ent_model = DecisionTreeClassifier(criterion='entropy')
ent_model.fit(x_train, y_train)
y_ent_model = ent_model.predict(x_test)
ent_acc = accuracy_score(y_test, y_ent_model)
print(f'ENTROPY accuracy ID3: {ent_acc * 100:.2f}%')

# plt.figure(figsize=(12,12))
# plot_tree(ent_model, filled=True, fontsize=6, feature_names=columns_name)
# plt.show()
#
# plt.figure(figsize=(12,12))
# plot_tree(gini_model, filled=True, fontsize=6, feature_names=columns_name)
# plt.show()
