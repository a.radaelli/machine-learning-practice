import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

raw_data = pd.read_csv('./data/regression.csv', header=0)
# print(raw_data)

# print(raw_data.isnull().sum())

np_data = raw_data.to_numpy(dtype='float')
print(np_data[6,:])

np_data=MinMaxScaler().fit_transform(np_data)
print(np_data[6,:])

#print(f'shape: {np_data.shape}')
x = np_data[:, :-1]
y = np_data[:, -1]

#print(x);print(y)

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)

regr = linear_model.LinearRegression()
regr.fit(x_train,y_train)
y_pred = regr.predict(x_test)

print("Coefficients: \n", regr.coef_)
# The mean squared error
print("Mean squared error: %.2f" % mean_squared_error(y_test, y_pred))
# The coefficient of determination: 1 is perfect prediction
print("Coefficient of determination: %.2f" % r2_score(y_test, y_pred))

df=pd.DataFrame(data=(y_test,y_pred))
print(df)
