# Machine Learning - Practice



## First thing first

This repository contains some of the practice script I wrote for my Machine Learning course 

## Description
- Breast Cancer, binary classification on the presence the probability to have breast cancer
- Diabetes, multiple classification on the degree of diabetes
- Regression, linear regression ML algorhytm
- Liver Disease, binary classification on the presence the probability to have liver disease
- Smoke, binary classification to tell if a person smokes or not
